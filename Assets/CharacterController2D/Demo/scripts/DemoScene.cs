﻿using UnityEngine;
using System.Collections;


public class DemoScene : MonoBehaviour
{
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;

	public int Health;

	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;

	private CharacterController2D _controller;
	private Animator _animator;
	private RaycastHit2D _lastControllerColliderHit;
	private Vector3 _velocity;

	// test
	[SerializeField]
	private Vector2 respawnPosition;

	[Range(0f, 1f)]
	public float ThresoldX;

	[Range(0f, 1f)]
	public float ThresoldY;

	// audio
	[SerializeField]
	private AudioClip dieSound;
	[SerializeField]
	private AudioClip jumpSound;
	[SerializeField]
	private AudioClip landSound;
	[SerializeField]
	private AudioClip shootSound;

	// projectiles
	[SerializeField]
	private GameObject projectile;

	#region properties
	public bool IsLeft {get;set;}
	public bool IsRight {get;set;}
	
	public bool IsJump {get;set;}
	public bool IsShoot {get;set;}

	public Vector2 SwipeAxis {get;set;}
	#endregion
	

	void Awake()
	{
		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();

		// listen to some events for illustration purposes
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		_controller.onTriggerExitEvent += onTriggerExitEvent;
	}


	#region Event Listeners
	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
		{
			if(_controller.collisionState.becameGroundedThisFrame) GetComponent<AudioSource>().PlayOneShot(landSound, 0.5f);
			return;
		}
		

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}


	void onTriggerEnterEvent( Collider2D col )
	{
		if(col.CompareTag("Soda")) return;
		GetComponent<AudioSource>().PlayOneShot(dieSound);
	}


	void onTriggerExitEvent( Collider2D col )
	{
		if(col.CompareTag("Soda")) return;
		transform.position = new Vector3(respawnPosition.x, respawnPosition.y, transform.position.z);
	}
	#endregion
	
	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;

		if( _controller.isGrounded )
		{
			_velocity.y = 0;
		}

		var padX = Input.GetAxisRaw("PaddleX");
		//var padY = Input.GetAxisRaw("PaddleY");


		if( Input.GetKey( KeyCode.RightArrow ) || padX > 0f || SwipeAxis.x > ThresoldX)
		{
			normalizedHorizontalSpeed = 1;
			if( transform.localScale.x < 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

			if( _controller.isGrounded && _animator != null)
				_animator.Play( Animator.StringToHash( "Run" ) );
		}
		else if( Input.GetKey( KeyCode.LeftArrow ) || padX < 0f || SwipeAxis.x < -ThresoldX)
		{
			normalizedHorizontalSpeed = -1;
			if( transform.localScale.x > 0f )
				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z );

			if( _controller.isGrounded && _animator != null )
				_animator.Play( Animator.StringToHash( "Run" ) );
		}
		else
		{
			normalizedHorizontalSpeed = 0;

			if( _controller.isGrounded && _animator != null )
				_animator.Play( Animator.StringToHash( "Idle" ) );
		}

		// we can only jump whilst grounded
		if( _controller.isGrounded && (Input.GetKeyDown( KeyCode.UpArrow ) || Input.GetButtonDown("Jump") || SwipeAxis.y > ThresoldY))
		{
			ResetSwipeAxisY();
			_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );

			GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.8f);

			if(_animator != null)
				_animator.Play( Animator.StringToHash( "Jump" ) );
		}
		else if(!_controller.isGrounded && SwipeAxis.y > 0f)
			ResetSwipeAxisY();

		// check for shooting
		if(Input.GetKeyDown(KeyCode.LeftControl) || Input.GetButtonDown("Fire") || IsShoot)
		{
			IsShoot = false;

			GetComponent<AudioSource>().PlayOneShot(shootSound, 1f);
			Shoot();
		}


		// apply horizontal speed smoothing it
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;

		_controller.move( _velocity * Time.deltaTime );

	}

	void ResetSwipeAxisY()
	{
		SwipeAxis = new Vector2(SwipeAxis.x, 0f);
	}

	void Shoot()
	{
		var instance = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
		instance.GetComponent<Projectile>().Throw(transform.localScale.x);
	}

}
