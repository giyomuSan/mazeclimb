﻿using UnityEngine;

namespace Platformer
{
    [CreateAssetMenu(fileName = "NewScriptableEnemy", menuName = "Platformer/Enemy", order = 2)]
    public class ScriptableEnemy : ScriptableObject
    {
        public string Name;
        public string Description;
        public int Health;
        public float RebirthTime;
        public AudioClip HitSound;
        public AudioClip DeathSound;
    }
}
