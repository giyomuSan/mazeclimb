﻿using UnityEngine;

namespace Platformer
{
    [CreateAssetMenu(fileName = "NewScriptableItem", menuName = "Platformer/Items", order = 1)]
    public class ScriptableItem : ScriptableObject
    {
        public string Name;
        public string Description;
        public int Value;
        public AudioClip PickupSound;
    }
}
