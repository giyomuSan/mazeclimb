using UnityEngine;
using System.Collections;

public class Enemyold : MonoBehaviour
{
	#region fields
	[SerializeField]
	private int health;
	[SerializeField]
	private float rebirthTime;
	[SerializeField]
	private LayerMask triggerMask;
	[SerializeField]
	private AudioClip hitSound;
	[SerializeField]
	private AudioClip deathSound;
	private Animator animator;
	private int hitHash = Animator.StringToHash("hit");

	private int currentHealth;
	#endregion

	#region methods
	void Start()
	{
		currentHealth = health;

		animator = GetComponent<Animator>();

		SetLayerCollision();
	}

	void SetLayerCollision()
	{
		for( var i = 0; i < 32; i++ )
		{
			// see if our triggerMask contains this layer and if not ignore it
			if( ( triggerMask.value & 1 << i ) == 0 )
				Physics2D.IgnoreLayerCollision( gameObject.layer, i );
		}
	}
	#endregion

	#region trigger event
	void OnTriggerEnter2D(Collider2D col)
	{
		if(--currentHealth <= 0)
		{
			SetVisible(false);
			StartCoroutine(Kill ());
		}
		else
		{
			GetComponent<AudioSource>().PlayOneShot(hitSound, 1f);
			animator.SetTrigger(hitHash);
		}
	}

	IEnumerator Kill()
	{
		var time = deathSound.length;
		GetComponent<AudioSource>().PlayOneShot(deathSound, 1f);

		yield return new WaitForSeconds(rebirthTime);

		SetVisible(true);
	}

	void SetVisible(bool isVisible)
	{
		currentHealth = health;

		GetComponent<Collider2D>().enabled = isVisible;
		GetComponent<Renderer>().enabled = isVisible;
	}
	#endregion
}

