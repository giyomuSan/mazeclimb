using UnityEngine;
using System;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace Downfall.Control
{
	public class UIControlMobileInput : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
	{
		#region fields
		[SerializeField]
		private float dragThresoldInX = 0.01f;
		[SerializeField]
		private float dragThresoldInY = 0.4f;
		[SerializeField]
		private float minDragTimeInY = 0.07f;

		private float timePressed;
		private Vector2 inputVector;
		#endregion

		#region event
		public event Action<Vector2> OnInputAxisEvent;
		#endregion

		#region methods
		public void OnBeginDrag (PointerEventData eventData)
		{
			timePressed = 0f;
		}
		
		public void OnDrag (PointerEventData eventData)
		{
			timePressed += Time.deltaTime;

			// x should always giving a zero > 0, also set y to 0 to avoid jump axis to trigger when sending event
			inputVector = new Vector2(eventData.delta.normalized.x + Mathf.Epsilon, 0f);

			if(OnInputAxisEvent != null && inputVector.x > dragThresoldInX || inputVector.x < -dragThresoldInX)
				OnInputAxisEvent(inputVector);
		}
		
		public void OnEndDrag (PointerEventData eventData)
		{
			// first check if we swipe fast enough otherwise return
			if(timePressed > minDragTimeInY) return;

			inputVector.y = eventData.delta.normalized.y;

			if(OnInputAxisEvent != null && inputVector.y > dragThresoldInY)
				OnInputAxisEvent(inputVector);

		}
		#endregion
	}
}

