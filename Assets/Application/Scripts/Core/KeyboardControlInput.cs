using UnityEngine;
using System;
using System.Collections;

namespace Downfall.Control
{
	public class KeyboardControlInput : MonoBehaviour
	{
		#region fields
		private Vector2 inputVector;
		#endregion

		#region event
		public event Action<Vector2> OnInputAxisEvent;
		#endregion

		#region methods
		void Update()
		{
			if(Input.GetAxisRaw("Horizontal") != 0f)
				inputVector.x = Input.GetAxisRaw("Horizontal");

			inputVector.y = Input.GetAxisRaw("Vertical");

			if(OnInputAxisEvent != null && inputVector != Vector2.zero)
				OnInputAxisEvent(inputVector);
		}
		#endregion
	}
}

