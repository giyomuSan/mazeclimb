using UnityEngine;
using System.Collections;
using Downfall.Control;

namespace Downfall
{
	public class PlayerControl : MonoBehaviour
	{
		#region fields
		public float Gravity = -80f;
		public float RunSpeed = 6f;
		public float GroundDamping = 20f; 
		public float InAirDamping = 2f;
		public float JumpHeight = 5f;

		public AudioClip JumpSound;
		
		[HideInInspector]
		private float normalizedHorizontalSpeed = 0;

		private Transform cachedTransform;
		private CharacterController2D controller;
		private AudioSource audioSource;
		private RaycastHit2D lastControllerColliderHit;
		private Vector3 velocity;
	
		// input
		private UIControlMobileInput inputController;
		private KeyboardControlInput keyboardInputController;

		private Vector2 inputAxis;
		#endregion

		#region event listeners
		void OnControlCollider(RaycastHit2D hit)
		{
			if(hit.normal.y == 1f)
			{
				// TODO : landing sound should be played here using becameGroundedThisFrame controller flag
				return;
			}
		}

		void OnTriggerEnterEvent(Collider2D col)
		{
			// TODO :  any trigger check with player and other element here
		}

		void OnTriggerExitEvent(Collider2D col)
		{
			// TODO : use it for respawn player when falling through screen ?
		}

		void OnInputAxisEvent(Vector2 vec)
		{
			inputAxis = vec;

			if(!controller.isGrounded) inputAxis.y = 0;
		}
		#endregion

		#region methods
		void Awake()
		{
			// caching
			cachedTransform = GetComponent<Transform>();
			controller = GetComponent<CharacterController2D>();
			audioSource = GetComponent<AudioSource>();

			// get input controller
			inputController = FindObjectOfType<UIControlMobileInput>();
			keyboardInputController = FindObjectOfType<KeyboardControlInput>();

			// event listener
			controller.onControllerCollidedEvent += OnControlCollider;
			controller.onTriggerEnterEvent += OnTriggerEnterEvent;
			controller.onTriggerExitEvent += OnTriggerExitEvent;
			inputController.OnInputAxisEvent += OnInputAxisEvent;
			keyboardInputController.OnInputAxisEvent += OnInputAxisEvent;
		}

		void OnDisable(){}

		void Update()
		{
			// grab our current _velocity to use as a base for all calculations
			velocity = controller.velocity;

			// keep track of our orientation
			var scaleInX = cachedTransform.localScale.x;

			if(controller.isGrounded) velocity.y = 0;

			// check for left right move
			if(inputAxis.x > 0f)
			{
				normalizedHorizontalSpeed = 1;
				if(scaleInX < 0f) cachedTransform.SetScaleX(-scaleInX);

				// TODO : running animation when move and grounded
			}
			else if(inputAxis.x < 0f)
			{
				normalizedHorizontalSpeed = -1;
				if(scaleInX > 0f) cachedTransform.SetScaleX(-scaleInX);
				
				// TODO : running animation when move and grounded
			}
			else
			{
				// for no movement
				normalizedHorizontalSpeed = 0;

				// TODO : idle animation when not move and grounded
			}

			// only jump when we are grounded
			if(controller.isGrounded && inputAxis.y > 0)
			{
				// reset it to avoid record jump during non grounded phase
				inputAxis.y = 0;

				velocity.y = Mathf.Sqrt(2f * JumpHeight * -Gravity);

				// TODO :  jump animation and jump sound
				audioSource.PlayOneShot(JumpSound, 1f);
			}

			// apply horizontal speed
			var smoothMovementFactor = controller.isGrounded ? GroundDamping : InAirDamping;
			velocity.x = Mathf.Lerp(velocity.x, normalizedHorizontalSpeed * RunSpeed, Time.deltaTime * smoothMovementFactor);

			// apply gravity before moving
			velocity.y += Gravity * Time.deltaTime;
			controller.move(velocity * Time.deltaTime);
		}
		#endregion
	}
}

