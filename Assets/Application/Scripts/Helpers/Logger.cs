using UnityEngine;
using System.Collections;

namespace Helpers
{
	/// <summary>
	/// Application debug log.
	/// Message will get colored regarding class, method and message passed
	/// for better visibility
	/// </summary>
	public static class Logger
	{
		#region fields
		private const int MESSAGE = 0;
		private const int ERROR = 1;

		private const string CLASS_COLOR = "<color=#74c0d9>";
		private const string METHOD_COLOR = "<color=#b5d1da>";
		private const string MESSAGE_COLOR = "<color=#ffb400>";
		private const string ERROR_COLOR = "<color=#fd8181>";
		private const string CLOSING_TAG = "</color>";
		#endregion

		#region methods
		public static void Message(string className, string methodName, string message)
		{
			#if UNITY_EDITOR
			var formatted = Format(ref className, ref methodName, ref message, MESSAGE);
			Debug.Log(formatted);
			#endif
		}

		public static void Message(Object className, string methodName, string message)
		{
			#if UNITY_EDITOR
			var formatted = Format(ref className, ref methodName, ref message, MESSAGE);
			Debug.Log(formatted);
			#endif
		}

		public static void Error(string className, string methodName, string message)
		{
			var formatted = Format(ref className, ref methodName, ref message, ERROR);
			Debug.LogError(formatted);
		}

		public static void Error(Object className, string methodName, string message)
		{
			var formatted = Format(ref className, ref methodName, ref message, ERROR);
			Debug.LogError(formatted);
		}
		#endregion

		#region helpers
		private static string Format(ref string className, ref string methodName, ref string message, int logType)
		{
			#if UNITY_EDITOR
			className = CLASS_COLOR + className + CLOSING_TAG;
			methodName = METHOD_COLOR + methodName + CLOSING_TAG;

			switch(logType)
			{
				case MESSAGE:
					message = MESSAGE_COLOR + message + CLOSING_TAG;
				break;
				case ERROR:
					message = ERROR_COLOR + message + CLOSING_TAG;
				break;
			default:
				message = MESSAGE_COLOR + message + CLOSING_TAG;
				break;
			}
			#endif

			return string.Format("{0}.{1}() : {2}", className, methodName, message);
		}

		private static string Format(ref Object className, ref string methodName, ref string message, int logType)
		{
			var classNameToString = className.GetType().ToString();
			return Format(ref classNameToString, ref methodName, ref message, logType);
		}
		#endregion
	}
}

