﻿using UnityEngine;
using EventMessage;

namespace Platformer
{
    public class HealthItem : Item
    {
        #region fields
        public const string OnGetHealth = "HealthItem.OnGetHealth";
        #endregion

        #region methods
        public override void Execute()
        {
            // Send message to any listeners
            MessageDispatcher.Instance.Send(OnGetHealth, scriptableItem.Value);

            // Sound
            GetComponent<AudioSource>().PlayOneShot(scriptableItem.PickupSound);

            SetVisible(false);
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            Execute();
        }
        #endregion

    }
}
