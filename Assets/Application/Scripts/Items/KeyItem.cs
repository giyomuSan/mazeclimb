using UnityEngine;
using EventMessage;

namespace Platformer
{
  public class KeyItem : Item
  {
      #region methods
      public override void Execute()
      {
          // TODO : Implement item behaviour when picked up here
      }

      protected override void OnTriggerEnter2D(Collider2D other)
      {
          Execute();
      }
      #endregion
  }
}
