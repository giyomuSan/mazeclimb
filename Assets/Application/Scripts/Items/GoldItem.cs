﻿using UnityEngine;
using EventMessage;

namespace Platformer
{
    public class GoldItem : Item
    {
        #region fields
        public const string OnGetGold = "GoldItem.OnGetGold";
        #endregion

        #region methods
        public override void Execute()
        {
            // Send message to any listeners
            MessageDispatcher.Instance.Send(OnGetGold, scriptableItem.Value);

            // Sound
            GetComponent<AudioSource>().PlayOneShot(scriptableItem.PickupSound);

            SetVisible(false);
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            Execute();
        }
        #endregion
    }
}
