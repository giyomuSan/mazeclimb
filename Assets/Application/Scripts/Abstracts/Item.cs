﻿using System;
using UnityEngine;

namespace Platformer
{
    public abstract class Item : MonoBehaviour, IPickable
    {
        #region fields
        [SerializeField] protected ScriptableItem scriptableItem;
        [SerializeField] protected LayerMask triggerMask;
        #endregion

        #region methods
        protected virtual void Start()
        {
            SetLayerCollision();
        }

        protected void SetLayerCollision()
        {
            for (var i = 0; i < 32; i++)
            {
                // see if our triggerMask contains this layer and if not ignore it
                if ((triggerMask.value & 1 << i) == 0)
                    Physics2D.IgnoreLayerCollision(gameObject.layer, i);
            }
        }

        protected void SetVisible(bool isVisible)
        {
            GetComponent<Collider2D>().enabled = isVisible;
            GetComponent<Renderer>().enabled = isVisible;
        }

        protected abstract void OnTriggerEnter2D(Collider2D other);
        #endregion

        #region interface implemntation
        public abstract void Execute();
        #endregion
    }
}
