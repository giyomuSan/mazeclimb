﻿using UnityEngine;
using System.Collections;

namespace Platformer
{
    public abstract class Enemy : MonoBehaviour
    {
        #region fields
        [SerializeField] protected ScriptableEnemy scriptableEnemy;
        [SerializeField] protected LayerMask triggerMask;
        [SerializeField] protected Animator animator;
        [SerializeField] protected int currentHealth;

        protected Vector2 spawnPosition;
        #endregion

        #region methods
        protected virtual void Start()
        {
            currentHealth = scriptableEnemy.Health;
            spawnPosition = transform.position;

            animator = GetComponent<Animator>();
            SetLayerCollision();
        }

        protected void SetLayerCollision()
        {
            for (var i = 0; i < 32; i++)
            {
                // see if our triggerMask contains this layer and if not ignore it
                if ((triggerMask.value & 1 << i) == 0)
                    Physics2D.IgnoreLayerCollision(gameObject.layer, i);
            }
        }

        protected virtual void SetVisible(bool isVisible)
        {
            GetComponent<Collider2D>().enabled = isVisible;
            GetComponent<Renderer>().enabled = isVisible;
        }

        protected virtual void Respawn()
        {
            currentHealth = scriptableEnemy.Health;
            transform.position = spawnPosition;

            SetVisible(true);
        }

        protected abstract void OnTriggerEnter2D(Collider2D other);
        protected abstract IEnumerator Kill();
        #endregion
    }
}
