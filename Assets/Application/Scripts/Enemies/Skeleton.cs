using UnityEngine;
using System.Collections;

namespace Platformer
{
    public class Skeleton : Enemy
    {
        #region fields
        private int hitHash = Animator.StringToHash("hit");
        #endregion

        #region methods
        protected override void Start()
        {
            base.Start();
        }

        protected override void SetVisible(bool isVisible)
        {
            base.SetVisible(isVisible);
        }

        protected override void Respawn()
        {
            base.Respawn();
        }

        protected override IEnumerator Kill()
        {
            GetComponent<AudioSource>().PlayOneShot(scriptableEnemy.DeathSound);
            yield return new WaitForSeconds(scriptableEnemy.RebirthTime);

            Respawn();
            SetVisible(true);
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                if(--currentHealth <= 0)
                {
                    SetVisible(false);
                    StartCoroutine(Kill());
                }
                else
                {
                    GetComponent<AudioSource>().PlayOneShot(scriptableEnemy.HitSound);
                    animator.SetTrigger(hitHash);
                }
            }
        }
        #endregion
    }
}
