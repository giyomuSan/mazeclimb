﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace Platformer.Tools
{
    [CustomEditor(typeof(ScriptableEnemy))]
    public class ScriptableEnemyInspector : Editor
    {
        private ScriptableEnemy o;
        private string className;
        private const string path = "Assets/Application/Scripts/Enemies/";

        void Awake()
        {
            o = (ScriptableEnemy)target;
        }

        public override void OnInspectorGUI()
        {
            className = EditorGUILayout.TextField("Class Name", className);

            if (!string.IsNullOrEmpty(className))
            {
                if (GUILayout.Button("Generate Template Class"))
                {
                    GenerateTemplateClass();
                    AssetDatabase.Refresh();
                }
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            base.OnInspectorGUI();

            if (GUI.changed) EditorUtility.SetDirty(o);
        }

        void GenerateTemplateClass()
        {
            StreamWriter ws = new StreamWriter(path + className + ".cs");

            ws.WriteLine("using UnityEngine;");
            ws.WriteLine("using System.Collections;");
            ws.WriteLine("");
            ws.WriteLine("namespace Platformer");
            ws.WriteLine("{");
            ws.WriteLine("  public class " + o.Name + " : Enemy");
            ws.WriteLine("  {");
            ws.WriteLine("      #region methods");
            ws.WriteLine("      protected override void Start()");
            ws.WriteLine("      {");
            ws.WriteLine("          base.Start();");
            ws.WriteLine("      }");
            ws.WriteLine("");
            ws.WriteLine("      protected override void SetVisible(bool isVisible)");
            ws.WriteLine("      {");
            ws.WriteLine("          base.SetVisible(isVisible);");
            ws.WriteLine("      }");
            ws.WriteLine("");
            ws.WriteLine("      protected override void Respawn()");
            ws.WriteLine("      {");
            ws.WriteLine("          base.Respawn();");
            ws.WriteLine("      }");
            ws.WriteLine("");
            ws.WriteLine("      protected override IEnumerator Kill()");
            ws.WriteLine("      {");
            ws.WriteLine("           yield return null;");
            ws.WriteLine("      }");
            ws.WriteLine("");
            ws.WriteLine("      protected override void OnTriggerEnter2D(Collider2D other)");
            ws.WriteLine("      {");
            ws.WriteLine("      }");
            ws.WriteLine("      #endregion");
            ws.WriteLine("  }");
            ws.WriteLine("}");

            ws.Close();
        }
    }
}
