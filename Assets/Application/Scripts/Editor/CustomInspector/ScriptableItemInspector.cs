﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace Platformer.Tools
{
    [CustomEditor(typeof(ScriptableItem))]
    public class ScriptableItemInspector : Editor
    {
        private ScriptableItem o;
        private string className;
        private const string path = "Assets/Application/Scripts/Items/";

        void Awake()
        {
            o = (ScriptableItem)target;
        }

        public override void OnInspectorGUI()
        {
            className = EditorGUILayout.TextField("Class Name", className);

            if (!string.IsNullOrEmpty(className))
            {
                if (GUILayout.Button("Generate Template Class"))
                {
                    GenerateTemplateClass();
                    AssetDatabase.Refresh();
                }
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            base.OnInspectorGUI();

            if (GUI.changed) EditorUtility.SetDirty(o);
        }

        void GenerateTemplateClass()
        {
            StreamWriter ws = new StreamWriter(path + className + ".cs");

            ws.WriteLine("using UnityEngine;");
            ws.WriteLine("using EventMessage;");
            ws.WriteLine("");
            ws.WriteLine("namespace Platformer");
            ws.WriteLine("{");
            ws.WriteLine("  public class " + o.Name +"Item : Item");
            ws.WriteLine("  {");
            ws.WriteLine("      #region methods");
            ws.WriteLine("      public override void Execute()");
            ws.WriteLine("      {");
            ws.WriteLine("          // TODO : Implement item behaviour when picked up here");
            ws.WriteLine("      }");
            ws.WriteLine("");
            ws.WriteLine("      protected override void OnTriggerEnter2D(Collider2D other)");
            ws.WriteLine("      {");
            ws.WriteLine("          Execute();");
            ws.WriteLine("      }");
            ws.WriteLine("      #endregion");
            ws.WriteLine("  }");
            ws.WriteLine("}");

            ws.Close();
        }
    }
}
