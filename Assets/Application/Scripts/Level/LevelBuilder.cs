using UnityEngine;
using System.Collections;

namespace Downfall.Level
{
	/// <summary>
	/// Test for tilemap brick background
	/// </summary>
	public class LevelBuilder : MonoBehaviour
	{
		#region fields
		public GameObject[] Tiles;
		public Vector2 ScreenOriginCoord;
		public int TileWidthNumber = 10;
		public int TileHeightNUmber = 15;
		public int TileVariationFactor;

		// 32 pixel per unit in editor but tile are scaled to 64x64
		public int UnitFactor = 2;
		#endregion

		#region methods
		void Start ()
		{
			if(Tiles == null || Tiles.Length <= 0) return;

			for(int x = 0; x < TileWidthNumber; x++)
			{
				for(int y = 0; y < TileHeightNUmber; y++)
				{
					var tileIndex = GetTileIndex();
					var tile = Instantiate(Tiles[tileIndex]) as GameObject;
					tile.transform.position = new Vector2( ScreenOriginCoord.x + (UnitFactor * x), ScreenOriginCoord.y - (UnitFactor * y));
				}
			}
		}

		private int GetTileIndex()
		{
			var range = Random.Range(0,100);

			if(range < TileVariationFactor) return Random.Range(0,2);
			else return Random.Range(2,4);
		}
		#endregion
	}
}

