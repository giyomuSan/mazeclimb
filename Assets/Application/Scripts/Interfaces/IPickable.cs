﻿namespace Platformer
{
    public interface IPickable
    {
        void Execute();
    }
}
