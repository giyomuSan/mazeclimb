﻿using UnityEngine;
using UnityEngine.UI;
using EventMessage;

namespace Platformer
{
    public class HealthTextInfo : MonoBehaviour, IMessageEvent
    {
        #region fields
        [SerializeField] protected int health = 100;
        #endregion

        #region methods
        void Start()
        {
            OnGetHealth(0);
            RegisterListener();
        }

        void OnDisable()
        {

        }
        #endregion

        #region event interface
        public void RegisterListener()
        {
            MessageDispatcher.Instance.AddListener<int>(HealthItem.OnGetHealth, OnGetHealth);
        }

        public void DeregisterListener()
        {
            MessageDispatcher.Instance.RemoveListener<int>(HealthItem.OnGetHealth, OnGetHealth);
        }
        #endregion

        #region event
        void OnGetHealth(int amount)
        {
            health += amount;

            var newText = "HP : " + health.ToString();
            GetComponent<Text>().text = newText;
        }

        #endregion
    }
}
