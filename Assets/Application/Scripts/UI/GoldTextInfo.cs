﻿using UnityEngine;
using UnityEngine.UI;
using EventMessage;
using System;

namespace Platformer
{
    public class GoldTextInfo : MonoBehaviour, IMessageEvent
    {
        #region fields
        [SerializeField] protected int gold = 0;
        #endregion

        #region methods
        void Start()
        {
            OnGetGold(0);
            RegisterListener();
        }

        void OnDisable()
        {

        }
        #endregion

        #region interface
        public void DeregisterListener()
        {
            MessageDispatcher.Instance.RemoveListener<int>(GoldItem.OnGetGold, OnGetGold);
        }

        public void RegisterListener()
        {
            MessageDispatcher.Instance.AddListener<int>(GoldItem.OnGetGold, OnGetGold);
            
        }
        #endregion

        #region event
        void OnGetGold(int amount)
        {
            gold += amount;

            var newText = "GOLD : " + gold.ToString();
            GetComponent<Text>().text = newText;
        }
        #endregion
    }
}
