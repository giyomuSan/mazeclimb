﻿using UnityEngine;
using EventMessage;

namespace Platformer
{

    public class PlayerMessageListener : MonoBehaviour, IMessageEvent
    {
        #region methods
        void Start()
        {
            RegisterListener();
        }

        void OnDisable()
        {
        }
        #endregion

        #region event interface
        public void RegisterListener()
        {
            MessageDispatcher.Instance.AddListener<int>(GoldItem.OnGetGold, OnGetGold);
            MessageDispatcher.Instance.AddListener<int>(HealthItem.OnGetHealth, OnGetHealth);
        }

        public void DeregisterListener()
        {
            MessageDispatcher.Instance.RemoveListener<int>(GoldItem.OnGetGold, OnGetGold);
            MessageDispatcher.Instance.RemoveListener<int>(HealthItem.OnGetHealth, OnGetHealth);
        }
        #endregion

        #region event message
        void OnGetGold(int amount)
        {
            Helpers.Logger.Message(this, "OnGetGold", "Gold received : " + amount.ToString());
        }

        void OnGetHealth(int amount)
        {
            Helpers.Logger.Message(this, "OnGetHealth", "Health received : " + amount.ToString());
        }
        #endregion
    }
}
