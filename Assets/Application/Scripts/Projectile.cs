using UnityEngine;
using System.Collections;


public class Projectile : MonoBehaviour
{
	#region fields
	[SerializeField]
	private float speed;
	[SerializeField]
	private LayerMask triggerMask;
	#endregion

	#region methods
	public void Throw(float direction)
	{
		SetLayerCollision();
		GetComponent<Rigidbody2D>().AddForce(new Vector2(speed * direction, 0f), ForceMode2D.Impulse);
	}

	void SetLayerCollision()
	{
		for( var i = 0; i < 32; i++ )
		{
			// see if our triggerMask contains this layer and if not ignore it
			if( ( triggerMask.value & 1 << i ) == 0 )
				Physics2D.IgnoreLayerCollision( gameObject.layer, i );
		}
	}

	void FixedUpdate()
	{
		var v = GetComponent<Rigidbody2D>().velocity;
		v.x = Mathf.Clamp(v.x, -speed, speed);

		GetComponent<Rigidbody2D>().velocity = v;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		Destroy(gameObject);
	}
	#endregion
}

