﻿/*
*Author : Guillaume Letaeron
*/

namespace EventMessage
{
    /// <summary>
    /// Delegate signature used by dispatcher
    /// </summary>
    
    public delegate void Message();
    public delegate void Message<T>(T arg);
    public delegate void Message<T, U>(T arg0, U arg1);
    public delegate void Message<T, U, V>(T arg0, U arg1, V arg2);
}
