﻿/*
*Author : Guillaume Letaeron
*/

namespace EventMessage
{
    /// <summary>
    /// Base interface to inherit when making custom message event interface
    /// </summary>
    public interface IMessageEvent
    {
        /// <summary>
        /// Use this method to add listener to dispatcher
        /// </summary>
        void RegisterListener();

        /// <summary>
        /// Use this method to remove listener from dispatcher
        /// </summary>
        void DeregisterListener();
    }
}
