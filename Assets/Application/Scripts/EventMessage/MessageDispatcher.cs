﻿/*
*Author : Guillaume Letaeron
*/

using UnityEngine;
using System;
using System.Collections.Generic;

namespace EventMessage
{
    /// <summary>
    /// Manage and dispatch message event registered
    /// </summary>
    public class MessageDispatcher : MonoBehaviour
    {
        #region singleton
        private static MessageDispatcher instance;
        public static MessageDispatcher Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = FindObjectOfType<MessageDispatcher>() as MessageDispatcher;

                    if (instance == null)
                        instance = new GameObject("MessageDispatcher").AddComponent<MessageDispatcher>();
                }

                return instance;
            }
        }

        void OnApplicationQuit()
        {
            Destroy(instance);
        }
        #endregion

        #region fields
        private Dictionary<string, List<Delegate>> eventTable;
        #endregion

        #region methods
        void OnEnable()
        {
            if (eventTable == null)
                eventTable = new Dictionary<string, List<Delegate>>();
        }
        #endregion

        #region addlistener
        /// <summary>
        /// No argument listener message
        /// </summary>
        public void AddListener(string eventName, Message handler)
        {
            if(!eventTable.ContainsKey(eventName))
            {
                var list = new List<Delegate>();
                list.Add(handler);

                eventTable.Add(eventName, list);
            }
            else
            {
                eventTable[eventName].Add(handler);
            }
        }

        /// <summary>
        /// One argumenet listener message
        /// </summary>
        public void AddListener<T>(string eventName, Message<T> handler)
        {
            if (!eventTable.ContainsKey(eventName))
            {
                var list = new List<Delegate>();
                list.Add(handler);

                eventTable.Add(eventName, list);
            }
            else
            {
                eventTable[eventName].Add(handler);
            }

        }

        /// <summary>
        /// Two argument listener message
        /// </summary>
        public void AddListener<T, U>(string eventName, Message<T, U> handler)
        {
            if(!eventTable.ContainsKey(eventName))
            {
                var list = new List<Delegate>();
                list.Add(handler);

                eventTable.Add(eventName, list);
            }
            else
            {
                eventTable[eventName].Add(handler);
            }
        }

        /// <summary>
        /// Three argument listener message
        /// </summary>
        public void AddListener<T, U, V>(string eventName, Message<T, U, V> handler)
        {
            if (!eventTable.ContainsKey(eventName))
            {
                var list = new List<Delegate>();
                list.Add(handler);

                eventTable.Add(eventName, list);
            }
            else
            {
                eventTable[eventName].Add(handler);
            }
        }
        #endregion

        #region remove listener
        /// <summary>
        /// remove message from a specific event name list
        /// Be careful than calling this while the same list iterate will throw an error !
        /// </summary>
        public void RemoveListener(string eventName, Message handler)
        {
            if (!eventTable.ContainsKey(eventName)) return;

            Message message;

            foreach(var d in eventTable[eventName])
            {
                message = d as Message;
                if(message == handler)
                {
                    eventTable[eventName].Remove(message);
                    break;
                }
            }
        }

        /// <summary>
        /// remove one argument listener message from a specific event name list
        /// Be careful than calling this while the same list iterate will throw an error !
        /// </summary>
        public void RemoveListener<T>(string eventName, Message<T> handler)
        {
            if (!eventTable.ContainsKey(eventName)) return;

            Message<T> message;

            foreach (var d in eventTable[eventName])
            {
                message = d as Message<T>;
                if (message == handler)
                {
                    eventTable[eventName].Remove(message);
                    break;
                }
            }
        }

        /// <summary>
        /// remove two argument listener message from a specific event name list
        /// Be careful than calling this while the same list iterate will throw an error !
        /// </summary>
        public void RemoveListener<T, U>(string eventName, Message<T, U> handler)
        {
            if (!eventTable.ContainsKey(eventName)) return;

            Message<T, U> message;

            foreach (var d in eventTable[eventName])
            {
                message = d as Message<T, U>;
                if (message == handler)
                {
                    eventTable[eventName].Remove(message);
                    break;
                }
            }
        }

        /// <summary>
        /// remove three argument listener message from a specific event name list
        /// Be careful than calling this while the same list iterate will throw an error !
        /// </summary>
        public void RemoveListener<T, U, V>(string eventName, Message<T, U, V> handler)
        {
            if (!eventTable.ContainsKey(eventName)) return;

            Message<T, U, V> message;

            foreach (var d in eventTable[eventName])
            {
                message = d as Message<T, U, V>;
                if (message == handler)
                {
                    eventTable[eventName].Remove(message);
                    break;
                }
            }
        }
        #endregion

        #region send message
        public void Send(string eventName)
        {
            List<Delegate> list = new List<Delegate>();
            if(eventTable.TryGetValue(eventName, out list))
            {
                Message message;
                foreach(var d in list)
                {
                    message = d as Message;
                    if (message != null) message();
                }
            }
        }

        public void Send<T>(string eventName, T arg)
        {
            List<Delegate> list = new List<Delegate>();
            if(eventTable.TryGetValue(eventName, out list))
            {
                Message<T> message;
                foreach(var d in list)
                {
                    message = d as Message<T>;
                    if (message != null) message(arg);
                }
            }
        }

        public void Send<T,U>(string eventName, T arg1, U arg2)
        {
            List<Delegate> list = new List<Delegate>();
            if (eventTable.TryGetValue(eventName, out list))
            {
                Message<T, U> message;
                foreach (var d in list)
                {
                    message = d as Message<T, U>;
                    if (message != null) message(arg1, arg2);
                }
            }
        }

        public void Send<T, U, V>(string eventName, T arg1, U arg2, V arg3)
        {
            List<Delegate> list = new List<Delegate>();
            if (eventTable.TryGetValue(eventName, out list))
            {
                Message<T, U, V> message;
                foreach (var d in list)
                {
                    message = d as Message<T, U, V>;
                    if (message != null) message(arg1, arg2, arg3);
                }
            }
        }
        #endregion
    }
}
